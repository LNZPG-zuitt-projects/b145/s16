// test
console.log('Hello World');

// main activity
let numA = 7;
let numB = 23;

// sum
	let sum = numA + numB;
	console.log('The sum of the two numbers is: '+ sum);
// diff
	let difference = numA - numB;
	console.log('The difference of the two numbers is: '+ difference);
// product
	let product = numA * numB;
	console.log('The product of the two numbers is: '+ product);
// quotient
	let quotient = numA / numB;
	console.log('The quotient of the two numbers is: '+ quotient);

// 4. Using comparison operators print out a string in the console that will return a message if the sum is greater than the difference.
	let sumVSdiff = sum > difference;
	console.log('The sum is greater than the difference: ' + sumVSdiff);
// 5. Using comparison and logical operators print out a string in the console that will return a message if the product and quotient are both positive numbers.
	let	isProductPositive = true;
	let isQuotientPositive =  true;
	let prodANDquo = isProductPositive && isQuotientPositive;
	console.log('The product and quotient are positive numbers: ' + prodANDquo);


	// ANSWER Key:
	const arePositiveNumbers = (product > 0 && quotient > 0);
	console.log('The product and quotient are positive numbers: ' + arePositiveNumbers);

// 6. Using comparison and logical operators print out a string in the console that will return a message if one of the results is a negative number.
	let isSumPositive = true;
	let isDiffferencePositive = false;
	let opsAns =  isSumPositive || isDiffferencePositive || isQuotientPositive || isQuotientPositive;
	console.log('One of the results is negative: ' + opsAns);

	// ANSWER Key
	let isNegative = (sum < 0 || difference < 0 || product < 0 || quotient < 0);
	console.log('One of the results is negative' + isNegative);

// 7. Using comparison and logical operators print out a string in the console that will return a message if one of the results is zero.
	let isSumZero = false;
	let isDifferenceZero = false;
	let isProductZero = false;
	let isQuotientZero = false;
	let oneOfAnsZero = isSumZero || isDifferenceZero || isProductZero || isQuotientZero;
	console.log('All the results are not equal to zero: ' + !oneOfAnsZero);

	//ANSWER Key
	let notZero = sum != 0 || difference != 0 || product != 0 || quotient != 0;
	console.log('All the results are not equal to zero: ' + notZero)

