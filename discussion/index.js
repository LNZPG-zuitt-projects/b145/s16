console.log('Hello JS')

// [Section 1]  Assignment Operator
	// 1. Basic assignment operator ( = )
	// allows us to add the value of the right operand to a variable and assigns the result to the variable
	let assignmentNumber = 5;

	let message = 'This is the message';


	// 2. Addition assignment operator (+=)
	// the addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.
	// assignmentNumber = assignmentNumber + 2;
	console.log("Result of the operation: " + assignmentNumber);

	// shorthanded version
	assignmentNumber += 2;
	console.log("Result of the operation: " + assignmentNumber);

// [Section 1: Sub 2] Arithmetic Operators
	// (+,  -, *, /)
	// 3. Subtraction/Multiplication/Division Assignment Operator (- / * / '/' + =).
	assignmentNumber -= 3;
	console.log("Result of the operation: " + assignmentNumber);


	let x = 15;
	let y = 10;

	// addition (+)
	let sum = x + y;
	console.log(sum);

	// subtraction (-)
	let difference = x - y;
	console.log(difference);

	// multiplication
	let product = x * y;
	console.log(product);

	// division
	let quotient = x / y;
	console.log(quotient);

	// remainder (modulus '%')
	let remainder = x % y;
	console.log(remainder);

// [SUBSECTION] Multiple Operators and Parentheses
	// follows PEMDAS rule.

	let mdas =1 + 2 - 3 * 4 / 5;
	console.log(mdas);

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log(pemdas);

// [SECTION] Increment and Decrement (++ or --)
	
	let z = 1;

	// pre and post

	// * post - babalik sa dati then need mag console.log(z) muna befor ma-add


	// increment (++)

	// pre increment (syntax: ++variable)
	let preIncrement = ++z;
	console.log(preIncrement);

	console.log(z);

	// post increment (syntax: variable++)
	let postIncrement = z++;
	console.log(postIncrement);

	console.log(z);

	// decrement (--)

	// pre decrement (syntax: --variable)
	let preDecrement = --z;
	console.log(preDecrement);

	// post decrement (syntax: variable--)
	let postDecrement = z--;
	console.log(postDecrement);
	console.log(z);


// [SECTION] Type Coercion
	// concatenates diff data types

	let a = 1;
	let b = '2';

	console.log(typeof a); //number
	console.log(typeof b); //string
	
	// converted to string


	let coercion = a + b;
	console.log(coercion);
	console.log(typeof coercion); //concatenation

	let expC = 10 + true;
	console.log(expC);

	let n = 10;
	console.log(typeof n);
	let bool = true;
	console.log(typeof bool);

	// boolean 'true' = 1 in JS
	// boolean 'false' = 0 in JS

	let exam = true + false;
	console.log(exam);


	// Conversion Rules:
		//1. If atleast one operand is an object, it will be converted into a primitive value/data type.
		//2. After conversion, if atleast 1 operand is a string data type, the 2nd operand is converted into another string to perform concatenation.
		//3. In other cases where both operands are converted to numbers then an arithmetic operation is executed.

	let exam1 = 8 + null; // 8
	console.log(exam1);
	// null converted to '0'.

	let exam2 = 'B145' + null; // B145null
	console.log(exam2);
	// null converted to string

	let exam3 = 9 + undefined; // NaN
	console.log(exam3);
	let e = undefined;
	console.log(typeof e)


// [SECTION] Comparison Operators
	let name =  'Juan'

	// [SUB-SECTION] Equality Operators
	// convert andd compares operand with 2 diff data types
	console.log(1  == 1); //true
	console.log(1  == 2); //false
	console.log(1  == '1'); //true
	console.log(1  == true); //true
	console.log(1 == false); //false
	console.log(name = 'Juan'); //true 
	// console.log('Juan' == Juan); error because var not yet declared.

	//[SUB SECTION] Inequality Operator (!=)
	// check if not eqqual/have diff values
	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1') //false
	console.log(1 != true) //false
	let juan = 'juan';
	console.log('juan' != juan); //false

	// [SUBSECTION] "STRICT" Equality Operators (===)
	// checks if equal or have same value
	// also comapres if same data type

	console.log(1 === 1); //true
	console.log(1 === '1'); //false
	console.log(0 === false); //false

	// [SUBSECTION] "STRICT" Inequality Operators (!==)  -- '!' not equal
	// checks if not equal or have diff value
	// check both values and data types of both components/operands.

	console.log(1 !== 1); //false
	console.log(1 !== '1'); //true
	console.log(1 !== 2); //true
	console.log(0 !== false); //true


//Developer's tip: upon creating conditions or statement it is strongly recommended to use "STRICT" equality operators over "loose" equality operators because it will be easier for us to predetermine outcomes and results in any given scenario.


	//[SECTION] Relational Operators
	let priceA = 1800;
	let	prriceB = 1450;

	// less than operator
	console.log(priceA < prriceB); //false
	// greater than operator
	console.log(priceA > prriceB); //true

	console.log(priceA <= prriceB);

	let sRO = 150 <= 150;
	console.log(sRO);


//Developer's Tip: When writing down/selecting variables name that would describe/contain a boolean value. it is writing convention for developers to add a prefix of "is" or "are" together with the variable name to form a variable similar on how to answer a simple yes or no question.

	// is + Single = true;
	// are + Taken = false;


isLegalAge = true;
isRegistered = false;

// to Vote both req mus be met

// logical Value
// AND (&& Double Ampersand) all criteria must be met
let isAllowedToVote  = isLegalAge && isRegistered;
console.log('Is the personn allowed to Vote? ' +isAllowedToVote);

// OR (||) Double Pipe) atleast 1 criteria must be met in order to pass.
let isAllowedForVaccination = isLegalAge || isRegistered;
console.log('Did the person pass? '  + isAllowedForVaccination);

// NOT  (! - exclamation point)
	// convert or return opposite value
	let isTaken  = true;
	let isTalented = false;

	console.log(isTaken); //true
	console.log(!isTaken); //false
	console.log(!isTalented); //true






































